---
title: Movie UI - Updates
template: main-noparent.html
---

We implemented deletions from the main movie, actor and ratings lists, but we haven't implemented it for the lists in the display screens. For example, deleting cast/filmography entries from a movie or actor display.

And what about editing the fields in each of our entities?

Source code for examples is at
[https://gitlab.com/605-686/fall-2023-refresh/modules/movie-ui-3](https://gitlab.com/605-686/fall-2023-refresh/modules/movie-ui-3)
