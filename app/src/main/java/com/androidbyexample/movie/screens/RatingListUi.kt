package com.androidbyexample.movie.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movie.R
import com.androidbyexample.movie.components.Display
import com.androidbyexample.movie.repository.RatingDto

@Composable
fun RatingListUi(
    ratings: List<RatingDto>,
    onRatingClicked: (RatingDto) -> Unit,

    selectedIds: Set<String>,
    onSelectionToggle: (id: String) -> Unit,
    onClearSelections: () -> Unit,
    onDeleteSelectedRatings: () -> Unit,

    currentScreen: Screen,
    onSelectListScreen: (Screen) -> Unit,

    onResetDatabase: () -> Unit,
) {
    ListScaffold(
        titleId = R.string.ratings,
        items = ratings,
        onItemClicked = onRatingClicked,
        selectedIds = selectedIds,
        onSelectionToggle = onSelectionToggle,
        onClearSelections = onClearSelections,
        onDeleteSelectedItems = onDeleteSelectedRatings,
        currentScreen = currentScreen,
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase
    ) { rating ->
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(8.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Star,
                contentDescription = stringResource(id = R.string.rating),
                modifier = Modifier.clickable {
                    onSelectionToggle(rating.id)
                }
            )
            Display(text = rating.name)
        }
    }
}
