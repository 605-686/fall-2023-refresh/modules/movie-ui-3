package com.androidbyexample.movie.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movie.R
import com.androidbyexample.movie.components.Display
import com.androidbyexample.movie.components.Label
import com.androidbyexample.movie.repository.MovieDto
import com.androidbyexample.movie.repository.RatingWithMoviesDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RatingDisplayUi(
    id: String,
    fetchRating: suspend (String) -> RatingWithMoviesDto,
    onMovieClicked: (MovieDto) -> Unit,
) {
    var ratingWithMovies by remember { mutableStateOf<RatingWithMoviesDto?>(null) }
    LaunchedEffect(key1 = id) {
        withContext(Dispatchers.IO) {
            ratingWithMovies = fetchRating(id)
        }
    }
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = ratingWithMovies?.rating?.name ?: stringResource(R.string.loading))
                }
            )
        }
    ) { paddingValues ->
        ratingWithMovies?.let { ratingWithMovies ->
            Column(
                modifier = Modifier
                    .padding(paddingValues)
            ) {
                Label(textId = R.string.name)
                Display(text = ratingWithMovies.rating.name)
                Label(textId = R.string.description)
                Display(text = ratingWithMovies.rating.description)
                Label(
                    text = stringResource(
                        id = R.string.movies_rated,
                        ratingWithMovies.rating.name
                    )
                )

                List(
                    items = ratingWithMovies.movies.sortedBy { it.title },
                    onItemClicked = onMovieClicked,
                    selectedIds = emptySet(),
                    onSelectionToggle = {},
                    onClearSelections = {},
                    modifier = Modifier.weight(1f)
                ) { movie ->
                    Display(
                        text = movie.title
                    )
                }
            }
        }
    }
}
